/*
* Nazev souboru: proj4.c
* Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz
* Datum: 18. 12. 2009
* Projekt: Ceske razeni, projekt cislo 4 pro predmet IZP
* Popis programu: Program seradi data ze souboru podle anglickeho nazvu bud vzestupne
*                 nebo sestupne podle toho, jaky klic uzivatel zada.
*                 Serazena data se zapisi do souboru, jehoz nazev si opet urci uzivatel.
*/

/** Knihovny potrebne pro beh programu.**/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** Funkce, ktera po zavolani vypise napovedu pro tento program.**/
void help(void)
{
    printf("Program Ceske razeni (2009)\n"
          "Autor: Vera Mullerova \n"
          "**************************** \n"
          " Program seradi data ze souboru, ktery si uzivatel vybere, \n"
          " podle anglickeho nazvu bud vzestupne nebo sestupne podle toho, \n"
          " jaky klic uzivatel zada. \n"
          " Serazena data se zapisi do souboru, jehoz nazev si opet urci uzivatel.\n"
          " \n"
          " Popis parametru: \n"
          "     -h                                      zobrazi tuto napovedu \n"
          "     --klic+=NNN vstupni.txt vystupni.txt   razeni vzestupne\n"
          "     --klic-=NNN vstupni.txt vystupni.txt   razeni sestupne\n"
    );
}

/** Vyctovy typ errors, ve kterem jsou definovane chyby, ktere se mohou v programu objevit. **/
enum errors
{
    EROK, // bez chyby
    ERPARAM, // spatny parametr
    ERPARAMCOUNT, // spatny pocet zadanych parametru
    EROPEN, // chyba pri otevirani souboru
    ERCLOSE, // chyba pri zavirani souboru
    ERALLOC, // chyba pri alokaci pameti
    ERUNKNOW // neznama chyba
};

/** Definice pole s chybovymi hlaskami. **/
const char *ERRORS[] =
{
    "Vse v poradku.",
    "Spatne zadane parametry.",
    "Spatny pocet zadanych parametru.",
    "Vyskytla se chyba pri otevirani souboru.",
    "Vyskytla se chyba pri zavirani souboru.",
    "Vyskytla se chyba pri alokaci pameti.",
    "Neznama chyba."
};

/**
* Funkce vypisujici chybove hlasky.
* @param error Chyba z vyctu chyb.
**/
void statementError(int error)
{
    if (error < EROK || error >= ERUNKNOW) //pokud neni chybova hlaska mezi EROK a ERNEVIM, tak se stalo neco necekaneho
        error = ERUNKNOW; //a vypise se mi chybova hlaska ERNEVIM
    fprintf(stderr, "%s \n", ERRORS[error]); //jinak se vypise prislusna chyba, ktera v programu opravdu nastala
}

/**
* Funkce, ktera zavre pozadovany soubor a pripadne detekuje chybu pri zavirani souboru.
* @param file Soubor, ktery se ma zavrit.
* @param error Chyba.
**/
int closeFile(FILE *file)
{
    int error = EROK;
    if (fclose(file) == EOF)
        return error = ERCLOSE;
    else
        return error;
}

/**
* Struktura pro jeden radek ze souboru.
* czech - ceske slovo
* english - anglicke slovo
* wclass - slovesny druh
**/
typedef struct
{
    char czech[100];
    char english[100];
    char wclass[100];
} TData;

typedef struct item TItem;

/**
* Struktura pro polozky linearniho seznam
* data - jeden radek ze souboru
* *next - ukazatel na dalsi radek v seznamu
**/
struct item
{
    TData data;
    TItem *next;
};

/**
* Struktura pro linearni seznam
* *first - ukazatel na prvni polozku seznamu
* *last - ukazatel na posledni polozku v seznamu
**/
typedef struct
{
    TItem *first;
    TItem *last;
}TList;

/** Funkce na inicializaci seznamu.**/
void listIni(TList *list)
{
    list->first = NULL;
    list->last = NULL;
    return;
}

/**
* Funkce, ktera vlozi radek precteny ze souboru
* na prvni misto v seznamu.
* @param *list Ukazatel na seznam.
* @param row Radek precteny ze souboru.
**/
int insertItem(TList *list, TData row)
{
    int error = EROK;
    TItem *newItem; //pomocny radek pri pridavani radku
    if ((newItem = malloc(sizeof(TItem))) == NULL)
        return error = ERALLOC;
    newItem->data = row;
    newItem->next = list->first;
    list->first = newItem;
    return error;
}

/**
* Funkce, ktera vymaze zaznam z radku,
* aby se na jeho m�sto mohla zapsat data z dalsiho radku.
* @param *row Ukazatel na radek zaznamu.
**/
void eraseData(TData *row)
{
    unsigned int k = strlen(row->czech);
    for (unsigned int j=0; j<k; j++) //vymazani pole s ceskym slovem
        row->czech[j] = '\0';
    k = strlen(row->english);
    for (unsigned int m=0; m<k; m++) //vymazani pole s anglickym slovem
        row->english[m] = '\0';
    k = strlen(row->wclass);
    for (unsigned int n=0; n<k; n++) //vymazani pole se slovnim druhem
        row->wclass[n] = '\0';
}

/**
* Funkce, ktera vezme data ze souboru a zapise je postupne do seznamu.
* @param *list Ukazatel na seznam.
* @param argv Pole textovych retezcu s argumenty.
**/
int readData(TList *list, char *argv[])
{
    int error = EROK;
    FILE *filein;
    filein = fopen(argv[2], "r"); //otevreni souboru pro cteni zaznamu
    if (filein == NULL) //detekce chyby pri otevirani souboru
        return error = EROPEN;

    int h = 0, i = 0;
    char c;
    TData tmp = {.czech = "0", .english = "0", .wclass = "0"}; //vynulovani polozky, do ktere se bude zapisovat radek
    while((c=getc(filein)) != EOF)
    {
        if (c == '\n') //pokud dojdu na konec radku, tak vlozim radek do seznamu na jeho zacatek
        {
            TList hlist = *list;
            error = insertItem(&hlist, tmp); //vlozeni radku na prvni misto v seznamu
            *list = hlist;
            i = 0; h = 0; //vynuluji citatel stredniku a indexu, protoze budu zapisovat novy radek
            eraseData(&tmp); //smazani udaju z tmp, aby se tam mohl zapsat dalsi radek
        }
        else //podle toho, za jakym strednikem se nachazim, tak zapisuji jednotlive znaky do prislusnych poli
        {
            switch(h)
            {
                case 0: tmp.czech[i++] = c; break; //zapsani pismene do pole s ceskym slovem
                case 1: tmp.english[i++] = c; break; //zapsani pismene do pole s anglickym slovem
                case 2: tmp.wclass[i++] = c; break; //zapsani pismene do pole se slovnim druhem
                default: closeFile(filein); return error = ERUNKNOW; break; //v pripade chyby se nejdrive zavre soubor a pak se vrati chybove hlaseni
            }
        }
        if (c == ';') //pokud je znak strednik, prictu si jeho pocitatel a budu zapisovat do jineho pole opet od zacatku
        {
            h++; i = 0;
        }
    }
    closeFile(filein); //zavreni souboru
    return error;
}

/**
* Funkce, ktera zeradi data podle anglickeho slova vzestupne.
* @param *list Ukazatel na seznam.
**/
int sortEnglishUp(TList *list)
{
    int error = EROK;
    int count = 1; //podle teto promenne zjistuji, kolik vymen pri projduti seznamem nastalo a tim take zjistim, zda uz je seznam serazeny
    while (count != 0)
    {
        TItem *rownext = list->first; //druhy radek
        rownext = rownext->next;
        for (TItem *row = list->first; rownext != NULL; row = row->next)
        {
            char *prvni = row->data.english;
            char *druhy = rownext->data.english;
            if ((strcmp(prvni, druhy)) > 0) // pokud je vysledek porovnavani vetsi nez nula tak se prohodi
            {//cyklus porovnavani a vymenovani radku probiha dokud nedojdu na konec seznamu
                TItem *newItem; //pomocny radek pri prohazovani radku
                if ((newItem = malloc(sizeof(TItem))) == NULL)
                    return error = ERALLOC;
                newItem->data = row->data;
                row->data = rownext->data;
                rownext->data = newItem->data;
                count++; //pricte se mi toto prohozeni
            }
            rownext = rownext->next; //posunu se o radek dal
        }
        if (count == 1) //zjistuji, zda uz je seznam srovnany
            count = 0; //pokud ano, nastvim count na 0 a tim skonci cyklus a cela funkce
        else
            count = 1; //pokud ne, obet nastavim na pocatecni 1
    }
    return error;
}

/**
* Funkce, ktera zeradi data podle anglickeho slova sestupne.
* @param *list Ukazatel na seznam.
**/
int sortEnglishDown(TList *list)
{
    int error = EROK;
    int countr = 0;
    int count = 1; //podle teto promenne zjistuji, kolik vymen pri projduti seznamem nastalo a tim take zjistim, zda uz je seznam serazeny
    while (count != 0)
    {
        TItem *rownext = list->first; //druhy radek
        rownext = rownext->next;
        for (TItem *row = list->first; rownext != NULL; row = row->next)
        {//cyklus porovnavani a vymenovani radku probiha dokud nedojdu na konec seznamu
            char *prvni = row->data.english;
            char *druhy = rownext->data.english;
            if ((strcmp(prvni, druhy)) < 0) //pokud je porovnani mensi nez nula, tak se radky v seznamu prohodi
            {
                TItem *newItem; //pomocny radek pri prohazovani radku
                if ((newItem = malloc(sizeof(TItem))) == NULL)
                    return error = ERUNKNOW;
                newItem->data = row->data;
                row->data = rownext->data;
                rownext->data = newItem->data;
                count++; //pricte se mi toto prohozeni
            }
            rownext = rownext->next; //posunu se o radek dal
            countr++;
        }
        if (count == 1) //zjistuji, zda uz je seznam srovnany
            count = 0; //pokud ano, nastvim count na 0 a tim skonci cyklus a cela funkce
        else
            count = 1; //pokud ne, obet nastavim na pocatecni 1
    }
    return error;
}

/**
* Funkce, ktera zapise do vystupniho souboru srovnany seznam.
* @param *list - Ukazatel na seznam.
* @param argv Pole textovych retezcu s argumenty.
**/
int writeData(TList *list, char *argv[])
{
    int error = EROK;
    FILE *fileout;
    fileout = fopen(argv[3], "w"); //otevreni souboru pro zapis
    if (fileout == NULL) //detekce chyby pri otevirani souboru
        return error = EROPEN;
    for (TItem *row = list->first; row != NULL; row = row->next) //zapis jednotlivych radku v cyklu
        fprintf(fileout, "%s%s%s\n", row->data.czech, row->data.english, row->data.wclass);
    closeFile(fileout); //zavreni souboru
    return error;
}

/**
* Funkce, ktera nam vrati cislo podle toho, ktery parametr uzivatel zadal.
* 1 - parametr -h
* 2 - spatny pocet zadanych parametru
* 3 - razeni podle klice
* 4 - chybny 1. parametr
* @param argc Pocet argumentu.
* @param argv Pole textovych retezcu s argumenty.
*/
int findParam(int argc, char *argv[])
{
    if (argc == 2 && strcmp("-h", argv[1]) == 0) // napoveda
        return 1;
    else if (argc != 4) // spatny pocet parametru
        return 2;
    else if (argc == 4) // zjistuji spravnost prvniho parametru
    {
        char *params = argv[1];
        int h = 0;
        if (strncmp("--klic+=", argv[1], 8) == 0 || strncmp("--klic-=", argv[1], 8) == 0) // razeni vzestupne
            h = 1;
        else return 4;
        if (strlen(argv[1]) > 11) // overuji maximalni delku prvniho parametru
            return 4;
        if (strlen(argv[1]) <= 8)
            return 4;
        int len = strlen(argv[1]);
        for (int i=8; i<len; i++)
        {
            if (params[i] != 49 && params[i] != 50 && params[i] != 51) // v klici jsou jina cisla nez 1, 2, 3
                return 4;
            if (params[i] == params[i-1] || params[i] == params[i-2]) // v klici jsou dve stejna cisla
                return 4;
        }
        if (h == 1)
            return 3;
        else return 4;
    }
    else return 4; // chybny parametr
}

/**
* Hlavni funkce.
* @param argc Pocet argumentu.
* @param argv Pole textovych retezcu s argumenty.
**/
int main(int argc, char *argv[])
{
    int error = EROK;
    int param = findParam(argc, argv); //zjisteni parametru
    char *params = argv[1];
    TList list;

    switch(param)
    {
        case 1: help(); break;
        case 2: error = ERPARAMCOUNT; break;
        case 3: listIni(&list);
                error = readData(&list, argv);
                if (error != EROK)
                    break;
                if (params[6] == 43) //pokud je klic + => vzestupne
                    error = sortEnglishUp(&list);
                else if (params[6] == 45) //pokud je klic - => sestupne
                    error = sortEnglishDown(&list);
                else error = ERUNKNOW;
                if (error != EROK)
                    break;
                error = writeData(&list, argv);
                break;
        case 4: error = ERPARAM; break;
        default: error = ERUNKNOW; break;
    }

    if (error != EROK) //kontrola chybove hlasky a pripadne jeji vypsani, pokud se behem programu nejaka chyba vyskytla
    {
        statementError(error); //vypis chyboveho hlaseni
        return EXIT_FAILURE;
    }
    else
    {
        return EXIT_SUCCESS;
    }
}
