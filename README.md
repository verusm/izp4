# README #

Fourth programming task for the course Introduction to Programming Systems:

Program sort data from input file by English alphabet ascending or descending based on input parameter.
Sorted data are saved into a file with name specified by user as input parameter.

Compile project by command

```
#!c

make
```

Start program by command

```
#!c

main --klic=-1 inputFile outputFile
```

Use parameter -h to see a help.